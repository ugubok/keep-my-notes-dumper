# coding: utf-8
from __future__ import unicode_literals

import argparse
import datetime
import itertools
import logging
import os
import sqlite3
from time import mktime
from typing import List, Optional, Text

import attr
import six
from lxml import html

"""
Dump notes from "Keep My Notes" DB files
"""

logger = logging.getLogger(__name__)


@attr.s(slots=True)
class Folder(object):
    id = attr.ib()
    parent_id = attr.ib()
    name = attr.ib()
    ctime = attr.ib()
    deleted = attr.ib()

    folders = attr.ib(default=attr.Factory(list), init=False)
    notes = attr.ib(default=attr.Factory(list), init=False)

    def store_folder(self, folder):
        self.folders.append(folder)

    def store_note(self, note):
        self.notes.append(note)

    def items_count(self):
        return len(self.notes) + len(self.folders)


@attr.s(slots=True)
class Note(object):
    id = attr.ib()
    title = attr.ib()
    description = attr.ib()
    short_description = attr.ib()
    deleted = attr.ib()
    ctime = attr.ib()
    mtime = attr.ib()
    folder_id = attr.ib()
    folder_name = attr.ib()


class NoteStore(object):
    def __init__(self, filename):
        self.db = sqlite3.connect(filename)

    def _select(self, fields, table):
        if isinstance(fields, tuple):
            fields = ",".join(fields)

        q = self.db.execute("SELECT {} FROM {}".format(fields, table))
        result = q.fetchall()

        if len(result) == 1 and len(result[0]) == 1:
            return result[0][0]

        return result

    def _count(self, table):
        return self._select("COUNT(*)", table)

    def get_folders(self):
        folders = self._select(
            ("folder_id", "parent_folder_id", "folder_name", "created_date", "deleted"),
            "folder",
        )

        for dir_id, parent_id, dir_name, ctime, del_ in folders:
            yield Folder(
                id=dir_id,
                parent_id=parent_id,
                name=dir_name,
                ctime=datetime.datetime.fromtimestamp(ctime / 1000),
                deleted=del_,
            )

    def get_notes(self):
        # type: () -> List[Note]
        notes = self._select(
            (
                "note_id",
                "title",
                "description",
                "short_description",
                "deleted",
                "created_date",
                "modified_date",
                "folder_id",
                "folder_name",
            ),
            "note",
        )

        for id_, title, desc, s_desc, del_, ctime, mtime, dir_id, dir_name in notes:
            yield Note(
                id=id_,
                title=title,
                description=desc,
                short_description=s_desc,
                deleted=bool(del_),
                ctime=datetime.datetime.fromtimestamp(ctime / 1000),
                mtime=datetime.datetime.fromtimestamp(mtime / 1000),
                folder_id=dir_id,
                folder_name=dir_name,
            )

    @property
    def attachments_count(self):
        return self._count("attachment")

    @property
    def checklist_count(self):
        return self._count("checklist")

    @property
    def folders_count(self):
        return self._count("folder")

    @property
    def handwritings_count(self):
        return self._count("handwriting")

    @property
    def notes_count(self):
        return self._count("note")

    @property
    def reminders_count(self):
        return self._count("reminder")

    @property
    def voice_records_count(self):
        return self._count("voice_recording")


class NotesFileSystemDumper(object):
    """
    Dump notes to FS. Make real directories structure and save each note as file
    """

    max_file_name_len = 32  # without file extension length
    note_file_ext = ""

    def __init__(self, notes_store, dest_path, overwrite=True):
        # type: (NoteStore, Text, Optional[bool]) -> NotesFileSystemDumper
        self.store = notes_store
        self.dir = dest_path
        self.overwrite = overwrite

    def _create_dest_dir(self):
        """
        Recursively creates destination directory
        """
        if os.path.exists(self.dir):
            if os.path.isdir(self.dir):
                return
            raise ValueError("Destination path exists, but it's not a directory")

        os.makedirs(self.dir)

    @staticmethod
    def _get_safer_name(name):
        """
        Replaces bad symbols in file name, so it can be saved in EXT filesystem
        That function doesn't care about any other FS like NTFS or FAT!
        """
        # If name is just a dot, hack it adding trailing space
        if name == ".":
            return ". "

        if name == "..":
            return ".. "

        name = name.replace("\n", "")

        # Replace all REAL slashes with unicode "DEVISION SLASH"
        name = name.replace("/", "\u2215")

        # Replace NULLs with it's \x-representation
        name = name.replace("\0", r"\x00")

        return name

    def _get_next_free_filename(self, name, path):
        # type: (Text, Text) -> Text
        """
        Check that given name not exists in path.
        If it exists, append number to given name
        """
        if not os.path.exists(os.path.join(path, name)):
            return name

        # Remove extension
        if self.note_file_ext:
            ext = "." + self.note_file_ext
            if name.endswith(ext):
                name = name[: -len(ext)]

        for count in itertools.count(start=2):
            new_name = "{} ({})".format(name, count)

            if self.note_file_ext:
                new_name += "." + self.note_file_ext

            if not os.path.exists(os.path.join(path, new_name)):
                return new_name

    @staticmethod
    def _render_note_contents(note):
        # type: (Note) -> Text
        lines = ["Created: {:%d %b %Y %H:%M}".format(note.ctime)]

        if note.ctime != note.mtime:
            date_interval = note.mtime - note.ctime

            if date_interval.days > 0:
                date_interval = "{} days after".format(date_interval.days)
            elif date_interval.seconds > 60 * 60:
                date_interval = "{:.0f} hours after".format(date_interval.seconds / 60 / 60)
            else:
                date_interval = "{:.0f} minutes after".format(date_interval.seconds / 60)

            lines.append("Modified: {:%d %b %Y %H:%M} ({})".format(note.mtime, date_interval))

        if note.title:
            lines.append("Title: {}".format(note.title))

        if note.folder_name:
            lines.append("Folder: {}".format(note.folder_name))

        lines.append("")
        lines.append(html.fromstring(note.description).text_content())

        return "\n".join(lines)

    @staticmethod
    def _change_file_mtime(path, mtime):
        # type: (Text, datetime.datetime) -> None
        ctime = mktime(mtime.timetuple())
        os.utime(path, (ctime, ctime))

    def build_root_fs(self):
        root = Folder(
            id=None,
            parent_id=None,
            name="",
            ctime=datetime.datetime.now(),
            deleted=False,
        )

        removed_folders = Folder(
            id=None,
            parent_id=None,
            name="(removed)",
            ctime=datetime.datetime.now(),
            deleted=False,
        )

        id2folder = {folder.id: folder for folder in self.store.get_folders()}

        # Save all notes to it's folder if it exists; save it to root folder otherwise
        for note in self.store.get_notes():
            note_folder = id2folder.get(note.folder_id, root)
            note_folder.store_note(note)

        for folder_id, folder in six.iteritems(id2folder):  # type: (int, Folder)
            parent_folder = root

            if folder.parent_id is not None:
                parent_folder = id2folder.get(folder.parent_id, root)
                if parent_folder is root:
                    logger.warning(
                        "Folder #{} is orphaned: parent folder #{} doesn't exists".format(
                            folder_id, folder.parent_id
                        )
                    )

            if folder.deleted and folder.parent_id is None:
                parent_folder = removed_folders

            parent_folder.store_folder(folder)

        if removed_folders.items_count() > 0:
            root.store_folder(removed_folders)

        return root

    def save_note(self, path, note):
        file_name = note.title or note.short_description or "Untitled"

        overflow_char = ""
        if len(file_name) > self.max_file_name_len:
            overflow_char = "…"

        file_name = "{}{}".format(file_name[: self.max_file_name_len], overflow_char)

        if self.note_file_ext:
            file_name += "." + self.note_file_ext

        file_name = self._get_safer_name(file_name)

        if note.deleted:
            file_name = "[del] " + file_name

        full_path = os.path.join(path, file_name)

        if os.path.exists(full_path):
            if self.overwrite:
                logger.warning("OVERWRITE " + full_path)
            else:
                file_name = self._get_next_free_filename(file_name, path)
                full_path = os.path.join(path, file_name)

        logger.debug("FILE " + full_path)

        with open(full_path, "w") as f:
            f.write(self._render_note_contents(note))

        self._change_file_mtime(full_path, note.ctime)

    def save_recursive(self, path, folder):
        # type: (Text, Folder) -> None
        dirname = self._get_safer_name(folder.name)

        if folder.deleted:
            dirname = "[del] " + dirname

        full_path = os.path.join(path, dirname)

        logger.debug("DIR " + full_path)

        if os.path.exists(full_path):
            if not os.path.isdir(full_path):
                raise ValueError("Destination path exists, but it's not a directory")
        else:
            os.mkdir(full_path)

        self._change_file_mtime(full_path, folder.ctime)

        for note in folder.notes:  # type: Note
            self.save_note(full_path, note)

        for nested_folder in folder.folders:
            self.save_recursive(full_path, nested_folder)

    def dump(self):
        root = self.build_root_fs()

        self._create_dest_dir()
        self.save_recursive(self.dir, root)


def check_and_show_warnings(store):
    # type: (NoteStore) -> None
    unsupported_entities_counts = {
        "attachments": store.attachments_count,
        "voice records": store.voice_records_count,
        "handwritings": store.handwritings_count,
        "reminders": store.reminders_count,
        "checklists": store.checklist_count,
    }

    for entity_name, count in six.iteritems(unsupported_entities_counts):
        if count > 0:
            logger.warning(
                "That file have {count} {entity_name}, that we can't dump yet! "
                "Please add {entity_name} dumping support".format(
                    entity_name=entity_name, count=count
                )
            )


def main():
    p = argparse.ArgumentParser()
    p.add_argument("file", help="Notes backup file")
    p.add_argument("-d", dest="dest_dir", help="Output directory")
    p.add_argument("--overwrite", action="store_true", help="Overwrite existing files")
    p.add_argument("-v", dest="verbose", action="store_true", help="Verbose output")
    args = p.parse_args()

    if not args.dest_dir:
        args.dest_dir, _ext = os.path.splitext(os.path.basename(args.file))
        args.dest_dir = os.path.abspath(args.dest_dir)

    loglevel = logging.DEBUG if args.verbose else logging.INFO
    logging.basicConfig(level=loglevel)

    store = NoteStore(args.file)

    check_and_show_warnings(store)

    dumper = NotesFileSystemDumper(store, args.dest_dir, overwrite=args.overwrite)
    dumper.dump()


if __name__ == "__main__":
    main()
